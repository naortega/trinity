/*
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

#include <stdio.h>

int main() {
	int God = 1;
	int *Father = &God;
	int *Son = &God;
	int *HolySpirit = &God;

	if(&God == Father)
		puts("The Father is God.");
	else
		puts("The Father is not God.");
	if(&God == Son)
		puts("The Son is God.");
	else
		puts("The Son is not God.");
	if(&God == HolySpirit)
		puts("The Holy Spirit is God.");
	else
		puts("The Holy Spirit is not God.");

	if(&Father == &Son)
		puts("The Father is the Son.");
	else
		puts("The Father is not the Son.");
	if(&Father == &HolySpirit)
		puts("The Father is the Holy Spirit.");
	else
		puts("The Father is not the Holy Spirit.");
	if(&Son == &HolySpirit)
		puts("The Son is the Holy Spirit.");
	else
		puts("The Son is not the Holy Spirit.");

	return 0;
}
